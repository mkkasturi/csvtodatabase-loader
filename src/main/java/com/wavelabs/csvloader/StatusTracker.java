package com.wavelabs.csvloader;

public class StatusTracker implements Runnable {

	private long totalRecords;
	private long processedRecords;

	DataQueue notifyQueue;

	public StatusTracker(DataQueue notifyQueue, long totalRecords) {
		this.notifyQueue = notifyQueue;
		this.totalRecords = totalRecords;
	}

	public StatusTracker(long totalRecords) {
		this.totalRecords = totalRecords;
		this.processedRecords = 0;
	}

	public void run() {
		Object batch = null;
		try {
			while ((batch = notifyQueue.dequeue()) != null) {

				if (batch instanceof Long) {
					processedRecords += ((Long) batch);
				} else if (batch instanceof Batch) {
					processedRecords += ((Batch) batch).getTotalRecords();
				}

				System.out.println(Thread.currentThread().getName() + ": records processed: " + processedRecords);
			}
		} catch (Exception x) {
			System.out.println(Thread.currentThread().getName() + ": Exception");
		}
	}

}
