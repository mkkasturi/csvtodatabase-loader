package com.wavelabs.csvloader;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class Worker implements Runnable {

	private Batch batch;
	private Status status;

	public Worker(Batch batch, Status status) {
		this.batch = batch;
		this.status = status;
	}

	public void run() {
		ArrayList<String> records = (ArrayList<String>) batch.getRecords();
		for (String record : records) {
			process(record);
		}
		status.incrementProcessedRecords(batch.getTotalRecords());
	}

	/*
	 * This method splits the records and stores the records in the database.
	 *
	 */
	private void process(String record) {

		String cvsSplitBy = ",";
		String[] details = record.split(cvsSplitBy);
		String movieName = details[0];
		int yearOfTheMovie = Integer.parseInt(details[1]);
		try {
			stmt = (PreparedStatement) connection.prepareStatement("insert into movie values(?,?)");
			stmt.setString(1, movieName);
			stmt.setInt(2, yearOfTheMovie);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	PreparedStatement stmt = null;
	static Connection connection = null;
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/imdb", "root", "root");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}