package com.wavelabs.csvloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CsvFileReader {
	int batchsize;
	BufferedReader br;

	CsvFileReader(String fileName, int batchsize) throws FileNotFoundException {
		this.batchsize = batchsize;
		br = new BufferedReader(new FileReader(new File(fileName)));
	}

	/*
	 * This method reads the file in Batch wise depending on the batch size
	 * passed.
	 */
	public Batch readBatch() throws IOException {
		Batch batch = new Batch();
		String line = "";

		int count = 0;
		while ((line = br.readLine()) != null) {
			batch.add(line);
			count++;
			if (count == batchsize) {
				break;
			}
		}
	
		return batch;
	}

	/*
	 * This method closes the buffered Reader
	 */
	public void closeResources() throws IOException {
		if (br != null) {
			br.close();
		}
	}
}
