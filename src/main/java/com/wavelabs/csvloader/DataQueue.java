package com.wavelabs.csvloader;

import java.util.LinkedList;
import java.util.List;

public class DataQueue {
	
	  private List queue = new LinkedList();
	  private int  limit;

	  public DataQueue(int limit){
	    this.limit = limit;
	  }

/*
 * This method writes the objects into the queue and waits if the queue is full.
 */
	  public synchronized void enqueue(Object item) throws InterruptedException  {
	   
		while(this.limit != -1 && this.queue.size() == this.limit) {
	      wait();
	    }
		
	    if(this.queue.size() == 0) {
	      notifyAll();
	    }
	    
	    this.queue.add(item);
	  }

/*
 * This method reads the objects from the queue
 */
	  public synchronized Object dequeue() throws InterruptedException{
	    while(this.queue.size() == 0){
	      wait();
	    }
	    
	    // i am removing an item from a Q that is full, 
	    // there could be threads waiting to enqueue an object, 
	    // lets notify them
	    if(this.queue.size() == this.limit){
	      notifyAll();
	    }

	    return this.queue.remove(0);
	  }

}
