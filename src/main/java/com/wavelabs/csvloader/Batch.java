package com.wavelabs.csvloader;

import java.util.ArrayList;
import java.util.List;

public class Batch {
	private List<String> records;

	public Batch() {
		records = new ArrayList<String>();
	}

	public List<String> getRecords() {
		return records;
	}

	public void setRecords(List<String> records) {
		this.records = records;
	}

	public void add(String line) {
		records.add(line);
	}

	public boolean isEmpty() {
		return records==null || records.isEmpty();
	}
	
	public long getTotalRecords() {
		return records.size();
	}
}
