package com.wavelabs.csvloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * This class reads the CSV file batch wise, create the tracker thread, create the workers in Thread pool.
 */
public class CsvLoaderApplication {

	public static void main(String[] args) {

		String filePath = args[0];
		int batchSize = Integer.parseInt(args[1]);

		try {
			CsvFileReader reader = new CsvFileReader(filePath, batchSize);
			ExecutorService executor = Executors.newFixedThreadPool(15);

			Status status = new Status();
			Batch batch = null;
			while ((batch = reader.readBatch()) != null && !batch.isEmpty()) {
				status.incrementPickedupRecordsCount(batchSize);
				Runnable worker = new Worker(batch, status);
				executor.execute(worker);
			}

			// wait for worker threads to finish
			executor.shutdown();
			while (!executor.isTerminated()) {
				try {
					Thread.currentThread().sleep(5000);
				} catch (InterruptedException e) {
				}
			}

			reader.closeResources();
		} catch (NumberFormatException e) {
		} catch (FileNotFoundException e) {
			System.err.println("The file - " + filePath + "does not exist.");
		} catch (IOException e) {
		}
		System.out.println("Done Processing");
	}
}

class Status {
	private long numberOfProcessedRecords = 0l;
	private long numberOfRecordsInFile = 0l;

	public synchronized void incrementProcessedRecords(long recordsprocessed) {
		numberOfProcessedRecords += recordsprocessed;
		double percentage = (numberOfProcessedRecords*100.0/numberOfRecordsInFile);
		System.out.println("Records processed so far batch wise - " + numberOfProcessedRecords);
		System.out.println(" Completion: "+percentage+" %");
	}
	
	public synchronized void incrementPickedupRecordsCount(long recordsPickedup) {
		numberOfRecordsInFile += recordsPickedup;
		System.out.println("Records assigned to process so far - " + numberOfRecordsInFile);
	}

}